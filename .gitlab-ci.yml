# This is adapted from GitLab's documentation at 
# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor
build docker image:
  tags: 
  - docker-privileged-xl
  # Use specific version of the official docker client image (as well as the docker-in-docker image) as recommended by
  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker
  # N.B.: make sure you use a pull-through cache mechanism to avoid hitting the pull limits in Docker:
  # - "Error response from daemon: toomanyrequests: You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limit"
  # ref: https://kubernetes.docs.cern.ch/docs/registry/quickstart/#pull-through-caches
  image: registry.cern.ch/docker.io/library/docker:20.10.16
  services:
  # To obtain a Docker daemon, request a Docker-in-Docker service
  # N.B.: make sure you use a pull-through cache mechanism to avoid hitting the pull limits in Docker:
  # - "Error response from daemon: toomanyrequests: You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limit"
  # ref: https://kubernetes.docs.cern.ch/docs/registry/quickstart/#pull-through-caches
  - name: registry.cern.ch/docker.io/library/docker:20.10.16-dind
    alias: docker
  variables:
      # As of GitLab 12.5, privileged runners at CERN mount a /certs/client docker volume that enables use of TLS to
      # communicate with the docker daemon. This avoids a warning about the docker service possibly not starting
      # successfully.
      # See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
      DOCKER_TLS_CERTDIR: "/certs"
      # Note that we do not need to set DOCKER_HOST when using the official docker client image: it automatically
      # defaults to tcp://docker:2376 upon seeing the TLS certificate directory.
      #DOCKER_HOST: tcp://docker:2376/
  before_script:
    # generate a simple Dockerfile
    - echo -e 'FROM alpine\nRUN echo aaa > somefile' > Dockerfile
  script:
    - docker info
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"

# The docker socket is not exposed on the network, so it is safe to disable TLS when using an older client
# in a custom client image (e.g. based on Alma9).
# This will however result in a warning that the docker-in-docker service may not have started successfully
# (though it did and is functional).
# This is because the health check is done on the TLS port 2376, which is not functional when TLS is disabled.
using a custom alma9-based image:
  tags: 
  - docker-privileged-xl
  # In this scenario we use a custom Docker image (Alma9) rather that the official docker client image
  image: gitlab-registry.cern.ch/linuxsupport/alma9-base
  services:
  # To obtain a Docker daemon, request a Docker-in-Docker service
  # N.B.: make sure you use a pull-through cache mechanism to avoid hitting the pull limits in Docker:
  # - "Error response from daemon: toomanyrequests: You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limit"
  # ref: https://kubernetes.docs.cern.ch/docs/registry/quickstart/#pull-through-caches
  - name: registry.cern.ch/docker.io/library/docker:20.10.16-dind
    alias: docker
  variables:
      # Disable TLS following https://about.gitlab.com/2019/07/31/docker-in-docker-with-docker-19-dot-03/
      # This will result in a warning that the docker-in-docker service may not have started successfully
      # (see above)
      DOCKER_TLS_CERTDIR: ""
      # When using dind service we need to instruct docker, to talk with the
      # daemon started inside of the service. The daemon is available with
      # a network connection instead of the default /var/run/docker.sock socket.
      #
      # The 'docker' hostname is the alias of the service container as described at
      # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
      #
      # In this scenario, we disable TLS and thus communicate with the Docker daemon on port 2375
      DOCKER_HOST: tcp://docker:2375/
  before_script:
    # install the docker CLI 
    - yum install -y docker
    # generate a simple Dockerfile
    - echo -e 'FROM alpine\nRUN echo aaa > somefile' > Dockerfile
  script:
    - docker info

# Demonstrate how to access CVMFS in privileged runners
cvmfs access:
  tags:
  - docker-privileged-xl
  # Use specific version of the official docker client image (as well as the docker-in-docker image) as recommended by
  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker
  # N.B.: make sure you use a pull-through cache mechanism to avoid hitting the pull limits in Docker:
  # - "Error response from daemon: toomanyrequests: You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limit"
  # ref: https://kubernetes.docs.cern.ch/docs/registry/quickstart/#pull-through-caches
  image: registry.cern.ch/docker.io/library/docker:20.10.16
  services:
  # To obtain a Docker daemon, request a Docker-in-Docker service
  # N.B.: make sure you use a pull-through cache mechanism to avoid hitting the pull limits in Docker:
  # - "Error response from daemon: toomanyrequests: You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limit"
  # ref: https://kubernetes.docs.cern.ch/docs/registry/quickstart/#pull-through-caches
  - name: registry.cern.ch/docker.io/library/docker:20.10.16-dind
    alias: docker
  variables:
      # As of GitLab 12.5, privileged runners at CERN mount a /certs/client docker volume that enables use of TLS to
      # communicate with the docker daemon. This avoids a warning about the docker service possibly not starting
      # successfully.
      # See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
      DOCKER_TLS_CERTDIR: "/certs"
      # Note that we do not need to set DOCKER_HOST when using the official docker client image: it automatically
      # defaults to tcp://docker:2376 upon seeing the TLS certificate directory.
      #DOCKER_HOST: tcp://docker:2376/
  script:
    - docker info
    # Use the CVMFS automounter image from https://gitlab.cern.ch/vcs/cvmfs-automounter.
    # This is possible because (as of GitLab 12.5), privileged runners at CERN mount a /shared-mounts volume
    # that allows sharing mount points between containers.
    # Important! This will not automount CVMFS on the current docker container where this CI is running, but
    # new containers can be started that will have access to CVMFS from the /shared-mounts volume.
    - docker run -d --name cvmfs --pid=host --user 0 --privileged --restart always -v /shared-mounts:/cvmfsmounts:rshared gitlab-registry.cern.ch/vcs/cvmfs-automounter:master
    # Now automount CVMFS on a new docker container by adding volume /shared-mounts/cvmfs:/cvmfs:rslave
    - docker run --rm -v /shared-mounts/cvmfs:/cvmfs:rslave busybox ls -lrt /cvmfs/cms.cern.ch

# Demonstrate how to mount the build working directory in secondary containers
mount build directory in secondary containers:
  tags: 
  - docker-privileged-xl
  # Use specific version of the official docker client image (as well as the docker-in-docker image) as recommended by
  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker
  # N.B.: make sure you use a pull-through cache mechanism to avoid hitting the pull limits in Docker:
  # - "Error response from daemon: toomanyrequests: You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limit"
  # ref: https://kubernetes.docs.cern.ch/docs/registry/quickstart/#pull-through-caches
  image: registry.cern.ch/docker.io/library/docker:20.10.16
  services:
  # To obtain a Docker daemon, request a Docker-in-Docker service
  # N.B.: make sure you use a pull-through cache mechanism to avoid hitting the pull limits in Docker:
  # - "Error response from daemon: toomanyrequests: You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limit"
  # ref: https://kubernetes.docs.cern.ch/docs/registry/quickstart/#pull-through-caches
  - name: registry.cern.ch/docker.io/library/docker:20.10.16-dind
    alias: docker
  variables:
      # As of GitLab 12.5, privileged runners at CERN mount a /certs/client docker volume that enables use of TLS to
      # communicate with the docker daemon.
      # See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
      DOCKER_TLS_CERTDIR: "/certs"
      # Note that we do not need to set DOCKER_HOST when using the official docker client image: it automatically
      # defaults to tcp://docker:2376 upon seeing the TLS certificate directory.
      #DOCKER_HOST: tcp://docker:2376/
  script:
    - docker info
    # Mount the build working directory into a secondary container to perform some work
    - docker run --rm -v $CI_PROJECT_DIR:/workdir busybox sh -c 'ls /workdir'

